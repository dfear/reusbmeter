#include "about.h"
#include "ui_about.h"
#include "mainwindow.h"


About::About(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::About)
{
    ui->setupUi(this);
}

About::~About()
{
    delete ui;
}

void About::on_closeabout_clicked()
{
    this->close();
}
