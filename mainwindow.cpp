#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "about.h"
#include "btconnect.h"

QBluetoothDeviceDiscoveryAgent* Bluetooth::agent = new QBluetoothDeviceDiscoveryAgent();
QBluetoothSocket* Bluetooth::socket = new QBluetoothSocket(QBluetoothServiceInfo::RfcommProtocol);

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(Bluetooth::socket, &QBluetoothSocket::connected, this, &MainWindow::isConnected);
    connect(Bluetooth::socket, &QBluetoothSocket::disconnected, this, &MainWindow::isDisconnected);
}

/*
 *  TopBar Menu Items Listeners
 *
 *  File -> Quit
 *  Help -> About
 */

void MainWindow::on_actionQuit_triggered()
{
    QApplication::quit();
}

void MainWindow::on_actionAbout_triggered()
{
    About about;
    about.setModal(true);
    about.exec();
}

/*
 * Default deconstructor
 */

MainWindow::~MainWindow()
{
    delete ui;
}

/*
 * Activities when connected for controlling the device also shows status.
 */

void MainWindow::isConnected() {
    ui->connectionKLed->setColor(QColor(0,255,0,255));
    ui->groupBoxDC->setEnabled(true);
    ui->lightSlider->setEnabled(true);
    ui->nextButton->setEnabled(true);
    ui->previousButton->setEnabled(true);
    ui->rotateButton->setEnabled(true);
}

void MainWindow::isDisconnected() {
    ui->connectionKLed->setColor(QColor(255,0,0,255));
    ui->groupBoxDC->setDisabled(true);
}

void MainWindow::on_devicecon_clicked()
{
    BTConnect btconnect;
    btconnect.setModal(true);
    btconnect.exec();
}

void MainWindow::on_deviceDiscon_clicked()
{
    Bluetooth::socket->disconnectFromService();
}

void MainWindow::on_lightSlider_sliderMoved(int value)
{
    QByteArray brightnessSend;
    uint8_t brSend = 0xd0 + (uint8_t)value;
    brightnessSend.append(brSend);
    Bluetooth::socket->write(brightnessSend);
    ui->backLightVal->setText(QString::number(value));
}

void MainWindow::on_nextButton_clicked()
{
    Bluetooth::socket->write(QByteArray::fromHex("0xf1"));
}

void MainWindow::on_previousButton_clicked()
{
    Bluetooth::socket->write(QByteArray::fromHex("0xf3"));
}

void MainWindow::on_rotateButton_clicked()
{
    Bluetooth::socket->write(QByteArray::fromHex("0xf2"));
}
