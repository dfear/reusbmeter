#ifndef BTCONNECT_H
#define BTCONNECT_H

#include <QDialog>
#include <QBluetoothDeviceDiscoveryAgent>
#include <QBluetoothSocket>
#include <QListWidget>

namespace Ui {
class BTConnect;
}

class BTConnect : public QDialog
{
    Q_OBJECT

public:
    explicit BTConnect(QWidget *parent = nullptr);
    ~BTConnect();

private slots:
    void on_findDevice_clicked();

    void on_btDeviceList_itemClicked(QListWidgetItem *item);

    void on_cancelButton_clicked();

    void deviceDiscovered(const QBluetoothDeviceInfo &device);

private:
    Ui::BTConnect *ui;
};

#endif // BTCONNECT_H
